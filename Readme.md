# AAV
                     
### Description


### Video demonstration

### Supported platforms

AAV is designed for Volvo Sensus Connected Touch (SCT) media unit.

https://www.volvocars.com/my/support/topics/car-systems/sensus-connected-touch/

### Compilation

- Android Studio - 4.1.3
- Gradle - 4.10.1
- Gradle plugin - 3.3.1

### License

GNU GPLv3

Copyrights (c) 2021 Pāvels Reško

### Used software

OpenWeather: https://openweathermap.org/

### Remarks

This software is created for R&D purposes and may not work as expected by the original authors. 
You use this software at your own risk.

### Usefull links

- https://forum.xda-developers.com/t/volvo-sct-volvo-sensus-connected-touch-car-navi-audio.2449005/
- https://4pda.ru/forum/index.php?showtopic=619847&st=800
