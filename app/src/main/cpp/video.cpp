//
// Created by incogn1to on 25/04/21.
//

extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavfilter/avfilter.h>
#include <libavdevice/avdevice.h>
#include <libswresample/swresample.h>
#include <libswscale/swscale.h>
#include <libavutil/avutil.h>
#include <sys/time.h>
}

#include "video.h"

void video::init() {
    // initialize libav
    av_register_all();
    avformat_network_init();
}
