#include <android_native_app_glue.h>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <string>
#include "renderer.h"
#include "loclog.h"
#include "GLContext.h"
#include "shader.h"
#include "macro.h"

#define BYTES_PER_FLOAT 4
#define POSITION_COMPONENT_COUNT 2
#define TEXTURE_COORDINATES_COMPONENT_COUNT 2
#define STRIDE ((POSITION_COMPONENT_COUNT + TEXTURE_COORDINATES_COMPONENT_COUNT)*BYTES_PER_FLOAT)


static const char glVertexShader[] =
        "attribute vec4 vPosition;\n"
        "void main()\n"
        "{\n"
        "  gl_Position = vPosition;\n"
        "}\n";

static const char glFragmentShader[] =
        "precision mediump float;\n"
        "void main()\n"
        "{\n"
        "  gl_FragColor = vec4(0.0, 0.0, 1.0, 1.0);\n"
        "}\n";

// square
const GLfloat gVertices[] = {
        -0.5f, -0.5f, 0.0f, //left-bottom
        0.5f, -0.5f, 0.0f, //right-bottom
        -0.5f, 0.5f, 0.0f, //left-top
        0.5f, 0.5f, 0.0f, //right-top
};

Renderer::Renderer() {

}

Renderer::~Renderer() {
    _context.invalidate();
}

bool Renderer::init(ANativeWindow* window) {
    _active = _context.init(window);
    _program = createProgram(glVertexShader,glFragmentShader);
    _position = glGetAttribLocation(_program, "vPosition");
    glViewport(0, 0, _context.getScreenWidth(), _context.getScreenHeight());
    return _active;
}

GLuint Renderer::createProgram(const char* vertexSource, const char * fragmentSource)
{
    GLuint vertexShader = shader::loadShader(GL_VERTEX_SHADER, vertexSource);
    if (!vertexShader)
    {
        return 0;
    }
    GLuint fragmentShader = shader::loadShader(GL_FRAGMENT_SHADER, fragmentSource);
    if (!fragmentShader)
    {
        return 0;
    }
    GLuint program = glCreateProgram();
    if (program)
    {
        glAttachShader(program , vertexShader);
        glAttachShader(program, fragmentShader);
        glLinkProgram(program);
        GLint linkStatus = GL_FALSE;
        glGetProgramiv(program , GL_LINK_STATUS, &linkStatus);
        if( linkStatus != GL_TRUE)
        {
            GLint bufLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength)
            {
                char* buf = (char*) malloc(bufLength);
                if (buf)
                {
                    glGetProgramInfoLog(program, bufLength, NULL, buf);
                    LOGE("Could not link program:\n%s\n", buf);
                    free(buf);
                }
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    return program;
}

void Renderer::resume(ANativeWindow* window) {
    if (!_active) {
        _active = _context.resume(window);
    }
}

void Renderer::terminate() {
    _active = false;
    _context.invalidate();
}

void Renderer::suspend() {
    _active = false;
    _context.suspend();
}

void Renderer::updateScene() {
    if (_active) {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear (GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
        glUseProgram(_program);
        glVertexAttribPointer(_position, 3, GL_FLOAT, GL_FALSE, 0 ,gVertices);
        glEnableVertexAttribArray(_position);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
}

void Renderer::drawGraphics() {

}

void Renderer::presentGraphics() {
    if (_active) {
        _context.swap();
    }
}
