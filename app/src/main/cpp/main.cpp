//
// Created by Pāvels Reško on 03/04/21.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <android/sensor.h>
#include <android_native_app_glue.h>
#include <android/native_window.h>
#include <android/window.h>
#include "loclog.h"
#include "renderer.h"
#include "androidauto.h"

extern "C" {

struct data {
    Renderer renderer;
    AndroidAuto aa;
};

static int32_t handle_input(struct android_app *app, AInputEvent *event) {
    data *user = (data *) app->userData;
    switch (AInputEvent_getType(event)) {
        case AINPUT_EVENT_TYPE_MOTION: {
            const int x = AMotionEvent_getX(event, 0);
            const int y = AMotionEvent_getY(event, 0);
            LOGI("Received touch event[%d] x[%d] y[%d]", AKeyEvent_getKeyCode(event), x, y);
            user->aa.touch(x, y);
            return 1;
        }
        case AINPUT_EVENT_TYPE_KEY: {
            const int key = AKeyEvent_getKeyCode(event);
            LOGI("Received key event[%d]", key);
            user->aa.keypress(key);
            return 1;
        }
        case AINPUT_EVENT_TYPE_FOCUS: {
            LOGI("Received focus event[%d]", AKeyEvent_getKeyCode(event));
            return 1;
        }
        default: {
            return 1;
        }
    }
}

static void handle_cmd(struct android_app *app, int32_t cmd) {
    data *user = (data *) app->userData;
    switch (cmd) {
        case APP_CMD_INIT_WINDOW: {
            LOGI("APP_CMD_INIT_WINDOW[%d]", cmd);
            if (app->window != NULL) {
                user->renderer.init( app->window );
            }
            break;
        }
        case APP_CMD_TERM_WINDOW: {
            LOGI("APP_CMD_TERM_WINDOW[%d]", cmd);
            user->renderer.terminate();
            break;
        }
        case APP_CMD_GAINED_FOCUS: {
            LOGI("APP_CMD_GAINED_FOCUS[%d]", cmd);
            if (app->window != NULL) {
                user->renderer.resume( app->window );
            }
            break;
        }
        case APP_CMD_LOST_FOCUS: {
            LOGI("APP_CMD_LOST_FOCUS[%d]", cmd);
            user->renderer.suspend();
            break;
        }
        case APP_CMD_SAVE_STATE: {
            break;
        }
        default:
        {
            LOGI("cmd[%d]", cmd);
            break;
        }
    }
}

void android_main(struct android_app *state) {
    data d;
    state->userData = &d;
    state->onAppCmd = handle_cmd;
    state->onInputEvent = handle_input;
    ANativeActivity_setWindowFlags(state->activity,
                                   0 | AWINDOW_FLAG_FULLSCREEN | AWINDOW_FLAG_KEEP_SCREEN_ON,
                                   0);
    do {
        int fdesc = 0;
        int events = 0;
        struct android_poll_source *source; 
        int ident = ALooper_pollAll(0, &fdesc, &events, (void **) &source);

        if (ident >= 0) {
            LOGI("ident[%d] fdesc[%d] events[%d]", ident, fdesc, events);
            if (source) {
                source->process(state, source);
            }
        }

        d.renderer.updateScene();
        d.renderer.drawGraphics();
        d.renderer.presentGraphics();

    } while (!state->destroyRequested);
}

}