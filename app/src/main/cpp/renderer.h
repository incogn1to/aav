#ifndef AAV_RENDERER_H
#define AAV_RENDERER_H

#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <string>
#include "GLContext.h"

class Renderer{

public:
    Renderer();
    ~Renderer();
    bool init(ANativeWindow* window);
    void terminate();
    void updateScene();
    void drawGraphics();
    void presentGraphics();
    void suspend();
    void resume(ANativeWindow* window);

private:
    GLuint createProgram(const char* vertexSource, const char * fragmentSource);
    bool _active;
    GLuint _program;
    GLContext _context;
    GLuint _position;
};

#endif